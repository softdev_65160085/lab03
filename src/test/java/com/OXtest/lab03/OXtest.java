/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.OXtest.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ASUS
 */
public class OXtest {
    
    public OXtest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWinRow2_O_output_true(){
        String[][] table = {{"1","2","3"},{"O","O","O"},{"7","8","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
     @Test
    public void testCheckWinRow1_O_output_true(){
        String[][] table = {{"O","O","O"},{"4","5","6"},{"7","8","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow3_O_output_true(){
        String[][] table = {{"1","2","3"},{"4","5","6"},{"O","O","O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow3_X_output_true(){
        String[][] table = {{"1","2","3"},{"4","5","6"},{"X","X","X"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow2_X_output_true(){
        String[][] table = {{"1","2","3"},{"X","X","X"},{"7","8","9"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow1_X_output_true(){
        String[][] table = {{"X","X","X"},{"4","5","6"},{"7","8","9"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol1_X_output_true(){
        String[][] table = {{"X","2","3"},{"X","5","6"},{"X","8","9"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol2_X_output_true(){
        String[][] table = {{"1","X","3"},{"4","X","6"},{"7","X","9"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol3_X_output_true(){
        String[][] table = {{"1","2","X"},{"4","5","X"},{"7","8","X"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
     @Test
    public void testCheckWinCol1_O_output_true(){
        String[][] table = {{"O","2","3"},{"O","5","6"},{"O","8","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
     @Test
    public void testCheckWinCol2_O_output_true(){
        String[][] table = {{"1","O","3"},{"2","O","6"},{"3","O","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
     @Test
    public void testCheckWinCol3_O_output_true(){
        String[][] table = {{"1","2","O"},{"4","5","O"},{"7","8","O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
     @Test
    public void testCheckWinDiag_O_output_true(){
        String[][] table = {{"O","2","3"},{"4","O","6"},{"7","8","O"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinDiag_X_output_true(){
        String[][] table = {{"X","2","3"},{"4","X","6"},{"7","8","X"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinAntiDiag_O_output_true(){
        String[][] table = {{"1","2","O"},{"4","O","6"},{"O","8","9"}};
        String currentPlayer = "O";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinAntiDiag_X_output_true(){
        String[][] table = {{"X","2","X"},{"4","X","6"},{"X","8","9"}};
        String currentPlayer = "X";
        boolean result = OX.checkWin(table,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckDraw_output_false(){
        String[][] table = {{"O","X","O"},{"X","X","O"},{"X","8","X"}};
        boolean result = OX.checkDraw(table);
        assertEquals(false,result);
    }
    @Test
    public void testCheckDraw_output_true(){
        String[][] table = {{"O","X","O"},{"X","X","O"},{"X","O","X"}};
        boolean result = OX.checkDraw(table);
        assertEquals(false,result);
    }






}
