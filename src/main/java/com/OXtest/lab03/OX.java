/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.OXtest.lab03;

/**
 *
 * @author ASUS
 */
class OX {
     static boolean checkWin(String[][] table, String currentPlayer){
        if (checkRow(table,currentPlayer)){
         return true;   
        }else if(checkCol(table,currentPlayer)){
        return true;
        }else if(checkDiag(table,currentPlayer)){
        return true;
        }else if(checkAntiDiag(table,currentPlayer)){
        return true;
        }return false;
       
}
    static boolean checkDraw(String[][] table) {
    for (int row = 0; row < 3; row++) {
        for (int col = 0; col < 3; col++) {
            if (!isCellOccupied(table[row][col])) {
                return false; 
            }
        }
    }
    return true;
}

private static boolean isCellOccupied(String cellValue) {
    try {
        int cellNumber = Integer.parseInt(cellValue);
        return cellNumber >= 1 && cellNumber <= 9;
    } catch (NumberFormatException e) {
        return false; 
    }
}
    private static boolean checkRow(String[][] table,String currentPlayer){
        for (int row=0;row<3;row++){
            if(checkRow(table,currentPlayer,row)){
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkCol(String[][] table,String currentPlayer){
        for (int col=0;col<3;col++){
            if(checkCol(table,currentPlayer,col)){
                return true;
            }
        }
        return false;
    }
    
    private static boolean checkDiag(String[][] table,String currentPlayer){
        for (int i=0;i<3;i++){
            if(!table[i][i].equals(currentPlayer))
	return false;
        }return true;
    }
    
    private static boolean checkAntiDiag(String[][] table,String currentPlayer){
        for (int i=0;i<3;i++){
            if(!table[i][(3-1)-i].equals(currentPlayer))
	return false;
        }return true;
    }

    private static boolean checkRow(String[][] table, String currentPlayer,int row) {
        return table[row][0].equals(currentPlayer) && table[row][1].equals(currentPlayer) && table[row][2].equals(currentPlayer);
    }
    private static boolean checkCol(String[][] table, String currentPlayer,int col) {
        return table[0][col].equals(currentPlayer) && table[1][col].equals(currentPlayer) && table[2][col].equals(currentPlayer);
    }

    
}
