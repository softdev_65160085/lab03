/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.OXtest.lab03;

/**
 *
 * @author ASUS
 */
import java.util.*;
public class Lab03 {
    
    static String[][] table = {{"1","2","3"},{"4","5","6"},{"7","8","9"}};
    static String currentPlayer = "X";
    static int col,row;
    static int count = 0;
    
    private static void printWelcome(){
        System.out.println("Welcome to XO game");
    } 
    
    private static void printTable(){
        for(int i = 0;i<3;i++){
            for(int j =0;j<3;j++){
                System.out.print(table[i][j]+ " ");
            }
            System.out.println("");
        }
    }
    
    private static void printTurn() {
        System.out.println(currentPlayer+" turn");
    }
    
    private static void inputNumber(){
        Scanner sc = new Scanner(System.in);
        while(true){
        System.out.print("Please Input Number to Place :");
        int number = sc.nextInt();
        col = (number-1)%3;
        row = (number-1)/3;
        count++;
        if(!"X".equals(table[row][col]) && !"O".equals(table[row][col])){
            table[row][col] = currentPlayer;
            return;
        }
        }
    }
    
    private static void switchPlayer(){
        if ("X".equals(currentPlayer) ){
            currentPlayer = "O";
        } else {
            currentPlayer = "X";
        }
    }
    
    private static boolean isWin(){
        if(checkRow()){ 
        return true;
        }else if(checkCol()){
        return true;
        }else if(checkDiag()){
        return true;
        }else if(checkAntiDiag()){
        return true;
        }return false;
    }
    
    private static boolean checkRow(){
        for(int i=0;i<3;i++){
            if(!table[row][i].equals(currentPlayer)){
                return false;
          
            }
        }return true;
    }
    
    private static boolean checkCol(){
        for(int i=0;i<3;i++){
            if(!table[i][col].equals(currentPlayer)){
                return false;
            }
        }return true;
    }
    
    private static boolean checkDiag(){
        for(int i = 0; i < 3; i++){
	if(!table[i][i].equals(currentPlayer))
	return false;
        }return true;
    }
    
    private static boolean checkAntiDiag(){
        for(int i = 0; i < 3; i++){
	if(!table[i][(3-1)-i].equals(currentPlayer))
	return false;
        }return true;
    }
        
    private static void printWin(){
        System.out.println(currentPlayer + " win!");
    }
    
    private static boolean isDraw(){
        if(count != 9){
        } else {
            return true;
        }
    return false;
    }
    
    private static void printDraw(){
        System.out.println("It's Tie!");
    }
    public static void main(String[] args) {
        printWelcome();
        while (true){
        printTable();
        printTurn();
        inputNumber();
        if(isWin()){
            printTable();
            printWin();
            break;
        }if(isDraw()){
            printTable();
            printDraw();
            break;
        }
        switchPlayer();
        }
    }   
}

